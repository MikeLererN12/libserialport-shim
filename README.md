# README #

To add this serial shim debugging to an application using libserialport, do the following:
	In the application change the include file from libserialport.h to serialshim.h
	Add serialshim.c to the source file list compile and go.
 
Note: the debug facility includes the capabilities to both
   	1) Logging all calls to the console via printf's.
   	2) Log all errors to the syslog.
This is controlled via 2 defines
	PRINTDEBUG
	SYSLOGGING


### What is this repository for? ###

Adds a shim layer to libserial port to aid in debugging

