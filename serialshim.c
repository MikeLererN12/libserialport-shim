/*
 * serialshim.c
 *
 *  Created on: Nov 7, 2016
 *      Author: mike
 *
 *
 *      To add this serial shim debugging to an application using libserialport, do the following:
 *     		In the application change the include file from libserialport.h to serialshim.h
 *      	Add serialshim.c to the source file list compile and go.
 *
 *      Note: the debug facility includes the capabilities to both
 *      	1) Logging all calls to the console via printf's.
 *      	2) Log all errors to the syslog.
 *      This is controlled via 2 defines
 *      	PRINTDEBUG
 *      	SYSLOGGING
 */
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>

#include <libserialport.h>

// Note choose one of the below two expansions
#ifdef PRINTDEBUG
#define DBG(args...)	{printf(args);}
#else
#define DBG(args...)	{}
#endif

// Note choose one of the below two expansions
#ifdef SYSLOGGING
#define LOG(args...)	{syslog(LOG_MAKEPRI(LOG_LOCAL1,LOG_ERR),args);}
#else
#define LOG(args...)	{}
#endif

// Port Enumeration
enum sp_return shim_sp_get_port_by_name(const char *portname, struct sp_port **port_ptr){
	enum sp_return thisreturn = sp_get_port_by_name(portname,port_ptr);
	DBG("get_port_by_name name=%s portptr=%p return=%x \n",portname,*port_ptr,thisreturn);
	if (thisreturn < 0) LOG("get_port_by_name name=%s portptr=%p return=%x \n",portname,*port_ptr,thisreturn);
	return(thisreturn);
};


void shim_sp_free_port(struct sp_port *port){
	DBG("free_port portptr=%p \n",port);
	sp_free_port(port);
};

enum sp_return shim_sp_list_ports(struct sp_port ***list_ptr){
	enum sp_return thisreturn = sp_list_ports(list_ptr);

	if (thisreturn < 0) {
		DBG("list_ports error=%x \n",thisreturn);
		return(thisreturn);
	} else {
		DBG("list_ports OK - basepointer=%p device 0 ptr=%p \n",(void *)*list_ptr,(void *)**list_ptr);
	}
	if (thisreturn < 0) 	LOG("list_ports error=%x \n",thisreturn);
	return(0);


}

void shim_sp_free_port_list(struct sp_port **ports){
	DBG("free_port_list %p \n",ports);
	sp_free_port_list(ports);
};


enum sp_return shim_sp_copy_port(const struct sp_port *port, struct sp_port **copy_ptr){
	enum sp_return thisreturn = sp_copy_port(port,copy_ptr);
	DBG("copy port portptr=%p copyptr=%p result=%x \n",port,copy_ptr,thisreturn);
	if (thisreturn < 0) 	LOG("copy port portptr=%p copyptr=%p result=%x \n",port,copy_ptr,thisreturn);
	return(thisreturn);
};


// Port Handling
enum sp_return shim_sp_open(struct sp_port *port, enum sp_mode flags){
	enum sp_return thisreturn = sp_open(port,flags);
	DBG("open portptr=%p  flags=%x result=%x \n",port,flags,thisreturn);
	if (thisreturn < 0) 	LOG("open portptr=%p  flags=%x result=%x \n",port,flags,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_close(struct sp_port *port){
	enum sp_return thisreturn = sp_close(port);
	DBG("close portptr=%p result=%x \n",port,thisreturn);
	if (thisreturn < 0) 	LOG("close portptr=%p result=%x \n",port,thisreturn);
	return(thisreturn);
}

char *shim_sp_get_port_name(const struct sp_port *port){
	char *thisreturn = sp_get_port_name(port);
	DBG("get_port_name portptr=%p  name=%s \n",port,thisreturn);
	if (thisreturn < 0)		LOG("get_port_name portptr=%p  name=%s \n",port,thisreturn);
	return(thisreturn);
}

char *shim_sp_get_port_description(const struct sp_port *port){
	char *thisreturn = sp_get_port_description(port);
	DBG("get_port_description portptr=%p description=%s \n",port,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_description portptr=%p description=%s \n",port,thisreturn);
	return(thisreturn);
}

enum sp_transport shim_sp_get_port_transport(const struct sp_port *port){
	enum sp_transport thisreturn = sp_get_port_transport(port);
	DBG("get_port_transport portptr=%p transport=%x \n",port,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_transport portptr=%p transport=%x \n",port,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_get_port_usb_bus_address(const struct sp_port *port,
                                           int *usb_bus, int *usb_address){
    enum sp_return thisreturn =   sp_get_port_usb_bus_address(port,usb_bus,usb_address);
    DBG("get_port_usb_bus_address portptr=%p usb_bus=%x usb_address=%x \n",port,*usb_bus,*usb_address);
    if (thisreturn < 0) 	LOG("get_port_usb_bus_address portptr=%p usb_bus=%x usb_address=%x \n",port,*usb_bus,*usb_address);
	return(thisreturn);
}

enum sp_return shim_sp_get_port_usb_vid_pid(const struct sp_port *port, int *usb_vid, int *usb_pid){
	enum sp_return thisreturn = sp_get_port_usb_vid_pid(port,usb_vid,usb_pid);
	DBG("get_port_usb_vid_pid portptr=%p usbvid=%x usbpid=%x result=%x \n",(void *)port,*usb_vid,*usb_pid,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_usb_vid_pid portptr=%p usbvid=%x usbpid=%x result=%x \n",(void *)port,*usb_vid,*usb_pid,thisreturn);
	return(thisreturn);
}

char *shim_sp_get_port_usb_manufacturer(const struct sp_port *port){
	char *thisreturn = sp_get_port_usb_manufacturer(port);
	DBG("get_port_usb_manufacturer port=%p manufacturer=%s \n",(void *)port,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_usb_manufacturer port=%p manufacturer=%s \n",(void *)port,thisreturn);
	return(thisreturn);
}

char *shim_sp_get_port_usb_product(const struct sp_port *port){
	char *thisreturn = sp_get_port_usb_product(port);
	DBG("get_port_usb_product %p %s \n",(void *)port,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_usb_product %p %s \n",(void *)port,thisreturn);
	return(thisreturn);
}

char *shim_sp_get_port_usb_serial(const struct sp_port *port){
	char *thisreturn = sp_get_port_usb_serial(port);
	DBG("get_port_usb_serial %p %s \n",(void *)port,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_usb_serial %p %s \n",(void *)port,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_get_port_handle(const struct sp_port *port, void *result_ptr){
	enum sp_return thisreturn = sp_get_port_handle(port,result_ptr);
	DBG("get_port_handle %p %p %x \n",(void *)port,(void *)result_ptr,thisreturn);
	if (thisreturn < 0) 	LOG("get_port_handle %p %p %x \n",(void *)port,(void *)result_ptr,thisreturn);
	return(thisreturn);
}

// Configuration
enum sp_return shim_sp_new_config(struct sp_port_config **config_ptr){
	enum sp_return thisreturn = sp_new_config(config_ptr);
	DBG("new config deviceptr=%p result=%x\n",(void *)*config_ptr,thisreturn);
	if (thisreturn < 0) 	LOG("new config deviceptr=%p result=%x\n",(void *)*config_ptr,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_set_config_baudrate(struct sp_port_config *config, int baudrate){
	enum sp_return thisreturn = sp_set_config_baudrate(config,baudrate);
	DBG("set baudrate device=%p  data=%x return=%x\n",(void *)config,baudrate,thisreturn);
	if (thisreturn < 0) 	LOG("set baudrate device=%p  data=%x return=%x\n",(void *)config,baudrate,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_set_config_bits(struct sp_port_config *config, int bits){
	enum sp_return thisreturn = sp_set_config_bits(config,bits);
	DBG("set_config_bits deviceptr=%p  bits=%x result=%x \n",(void *)config,bits,thisreturn);
	if (thisreturn < 0) 	LOG("set_config_bits deviceptr=%p  bits=%x result=%x \n",(void *)config,bits,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_set_config_parity(struct sp_port_config *config, enum sp_parity parity){
	enum sp_return thisreturn = sp_set_config_parity(config,parity);
	DBG("set_config_parity deviceptr=%p  parity=%x result=%x \n",(void *)config,parity,thisreturn);
	if (thisreturn < 0) 	LOG("set_config_parity deviceptr=%p  parity=%x result=%x \n",(void *)config,parity,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_set_config_stopbits(struct sp_port_config *config, int stopbits){
	enum sp_return thisreturn = sp_set_config_stopbits(config,stopbits);
	DBG("set_config_stopbits deviceptr=%p  stopbits=%x result=%x \n",(void *)config,stopbits,thisreturn);
	if (thisreturn < 0) 	LOG("set_config_stopbits deviceptr=%p  stopbits=%x result=%x \n",(void *)config,stopbits,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_set_config_flowcontrol(struct sp_port_config *config, enum sp_flowcontrol flowcontrol){
	enum sp_return thisreturn = sp_set_config_flowcontrol(config,flowcontrol);
	DBG("set_config_flowcontrol deviceptr=%p  flowcontrol=%x result=%x \n",(void *)config,flowcontrol,thisreturn);
	if (thisreturn < 0) 	LOG("set_config_flowcontrol deviceptr=%p  flowcontrol=%x result=%x \n",(void *)config,flowcontrol,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_set_config(struct sp_port *port, const struct sp_port_config *config){
	enum sp_return thisreturn = sp_set_config(port,config);
	DBG("set_config deviceptr=%p  port=%p config=%x \n",(void *)port,(void *)config,thisreturn);
	if (thisreturn < 0) 	LOG("set_config deviceptr=%p  port=%p config=%x \n",(void *)port,(void *)config,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_get_config_bits(const struct sp_port_config *config, int *bits_ptr){
	enum sp_return thisreturn = sp_get_config_bits(config,bits_ptr);
	DBG("get_config_bits configptr=%p bits=%x result=%x \n",(void *)config,*bits_ptr,thisreturn);
	if (thisreturn < 0) 	LOG("get_config_bits configptr=%p bits=%x result=%x \n",(void *)config,*bits_ptr,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_get_config_parity(const struct sp_port_config *config, enum sp_parity *parity_ptr){
	enum sp_return thisreturn = sp_get_config_parity(config,parity_ptr);
	DBG("get_config_parity configptr=%p parity=%x result=%x \n",(void *)config,*parity_ptr,thisreturn);
	if (thisreturn < 0) 	LOG("get_config_parity configptr=%p parity=%x result=%x \n",(void *)config,*parity_ptr,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_get_config_baudrate(const struct sp_port_config *config, int *baudrate_ptr){
	enum sp_return thisreturn = sp_get_config_baudrate(config,baudrate_ptr);
	DBG("get_config_baudrate configptr=%p baudrate=%x result=%x \n",(void *)config,*baudrate_ptr,thisreturn);
	if (thisreturn < 0) 	LOG("get_config_baudrate configptr=%p baudrate=%x result=%x \n",(void *)config,*baudrate_ptr,thisreturn);
	return(thisreturn);
}


// Data Handling
enum sp_return shim_sp_blocking_write(struct sp_port *port, const void *buf, size_t count, unsigned int timeout_ms){
	enum sp_return thisreturn = sp_blocking_write(port,buf,count,timeout_ms);
	DBG("blocking_write port=%p  bytecount=%lu timeout=%d dataptr=%p  result=%x \n",(void *)port,count,timeout_ms,(void *) buf,thisreturn);
	if (thisreturn < 0) 	LOG("blocking_write port=%p  bytecount=%lu timeout=%d dataptr=%p  result=%x \n",(void *)port,count,timeout_ms,(void *) buf,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_nonblocking_write(struct sp_port *port, const void *buf, size_t count){
	enum sp_return thisreturn = sp_nonblocking_write(port,buf,count);
	DBG("nonblocking_write port=%p  bytecount=%lu dataptr=%p  result=%x \n",(void *)port,count,(void *) buf,thisreturn);
	if (thisreturn < 0) 	LOG("nonblocking_write port=%p  bytecount=%lu dataptr=%p  result=%x \n",(void *)port,count,(void *) buf,thisreturn);
	return(thisreturn);
}


enum sp_return shim_sp_nonblocking_read(struct sp_port *port, void *buf, size_t count){
	enum sp_return thisreturn = sp_nonblocking_read(port,buf,count);
	DBG("nonblocking_read port=%p count=%lu  dataptr=%p result=%x \n",(void *)port,count,(void *) buf,thisreturn);
	if (thisreturn < 0) 	LOG("nonblocking_read port=%p count=%lu  dataptr=%p result=%x \n",(void *)port,count,(void *) buf,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_blocking_read(struct sp_port *port, void *buf, size_t count, unsigned int timeout_ms){
	enum sp_return thisreturn = sp_blocking_read(port,buf,count,timeout_ms);
	DBG("blocking_read port=%p count=%lu timeout=%d dataptr=%p result=%x \n",(void *)port,count,timeout_ms,(void *) buf,thisreturn);
	if (thisreturn < 0) 	LOG("blocking_read port=%p count=%lu timeout=%d dataptr=%p result=%x \n",(void *)port,count,timeout_ms,(void *) buf,thisreturn);
	return(thisreturn);
	}

enum sp_return shim_sp_blocking_read_next(struct sp_port *port, void *buf, size_t count, unsigned int timeout_ms){
	enum sp_return thisreturn = sp_blocking_read_next(port,buf,count,timeout_ms);
	DBG("blocking_read_next port=%p count=%lu timeout=%d dataptr=%p result=%x \n",(void *)port,count,timeout_ms,(void *) buf,thisreturn);
	if (thisreturn < 0) 	LOG("blocking_read_next port=%p count=%lu timeout=%d dataptr=%p result=%x \n",(void *)port,count,timeout_ms,(void *) buf,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_output_waiting(struct sp_port *port){
	enum sp_return thisreturn = sp_output_waiting(port);
	DBG("sp_output_waiting %p %x",port,thisreturn);
	if (thisreturn < 0) 	LOG("sp_output_waiting %p %x",port,thisreturn);
	return(thisreturn);
}

enum sp_return shim_sp_input_waiting(struct sp_port *port){
	enum sp_return thisreturn = sp_input_waiting(port);
	DBG("sp_input_waiting %p %x",port,thisreturn);
	if (thisreturn < 0) 	LOG("sp_input_waiting %p %x",port,thisreturn);
	return(thisreturn);
}


void shim_sp_free_config(struct sp_port_config *config){
	DBG("free_config config=%p \n",config);
	sp_free_config(config);
	return;
}

enum sp_return shim_sp_flush(struct sp_port *port, enum sp_buffer buffers){
	enum sp_return thisreturn = sp_flush(port,buffers);
	DBG("flush portptr=%p buffers=%p result=%x \n",(void *)port,(void *)buffers,thisreturn);
	if (thisreturn < 0) 	LOG("flush portptr=%p buffers=%p result=%x \n",(void *)port,(void *)buffers,thisreturn);
	return(thisreturn);
}
